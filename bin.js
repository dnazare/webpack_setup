#!/usr/bin/env node

var webpack_setup = require('./');

var mode = 'dev';
var help = false;
var open = false;
var analyze = false;
var watch = false;
process.argv.slice(2).map(function (arg) {
    if (arg === '--prod' || arg === '-P')
        mode = 'prod';
    else if (arg === '--test' || arg === '-T')
        mode = 'test';
    if (arg === '--open')
        open = true;
    if (arg === '--watch')
        watch = true;
    if (arg === '--analyze')
        analyze = true;
    else if (arg.match(/^(-+|\/)(h(elp)?|\?)$/))
        help = true;
})

if (help) {
    var log = help ? console.log : console.error
    log('Usage: webpack_setup <mode>')
    log('')
    log('  Starts webpack execution based on mode.')
    log('')
    log('Options:')
    log('')
    log('  -P, --prod     Production mode to create production build');
    log('  -D, --dev      Development mode (default)');
    log('  -T, --test      Development mode (default)');
    log('  -open          Open browser in development mode');
    log('  -analyze       Run analyzer');
    process.exit(0);
} else {
    open = open && mode === 'dev';
    webpack_setup({ mode, open, analyze, watch });
}

