'use strict';

const merge = require('webpack-merge');
const common = require('./webpack.common');
const qaConfig = require('./devserver.qa.config');

module.exports = merge(common(), {
  mode: 'development',

  output: {
    publicPath: 'http://localhost:8081/',
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js'
  },

  devtool: 'inline-source-map',

  devServer: qaConfig
});