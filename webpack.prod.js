'use strict';

const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var processPath = process.cwd();
var distPath = path.join(processPath, 'dist');

module.exports = merge(common(), {
	mode: 'production',

	output: {
		path: distPath,
		publicPath: '',
		filename: '[name].[hash].js',
		chunkFilename: '[name].[hash].js'
	},

	devtool: false,

	module: {
		rules: [
			{
				test: /src.*\.js$/,
				exclude: [
					/node_modules/,
					/\.spec\.js$/
				],
				use: [{ loader: 'ng-annotate-loader', options: { map: false } }],
			}
		]
	},

	plugins: [
		new CleanWebpackPlugin(['dist'], { root: processPath }),
		new webpack.NoEmitOnErrorsPlugin(),
		//new UglifyJsPlugin({ exclude: /src/ }),
		//new UglifyJsPlugin({ include: /src/, uglifyOptions: { mangle: false } }),
		//new BundleAnalyzerPlugin()
	],

	optimization: {
		minimize: true,
		runtimeChunk: {
			name: 'vendor'
		},
		splitChunks: {
			cacheGroups: {
				default: false,
				commons: {
					test: /node_modules/,
					name: "vendor",
					chunks: "initial",
					minSize: 1
				}
			}
		}
	}
});