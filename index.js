
module.exports = webpack_setup;

const path = require('path');
const exec = require('child_process').exec;

function webpack_setup(options) {
  var cmd;
  if (options.mode === 'prod') {
    cmd = 'webpack --progress --colors --config '
      + path.join(__dirname, 'webpack.prod.js');
    outRef = exec(cmd);
  } else if (options.mode === 'test') {
    cmd = 'karma start';
    cmd += ' ' + path.join(__dirname, 'karma.config.js');
    if (options.watch) {
      cmd += ' --auto-watch --no-single-run';
    }
    outRef = exec(cmd);
  } else {
    var cmd = 'webpack-dev-server --progress --colors --config '
      + ' ' + path.join(__dirname, 'webpack.dev.js')
      + (options.open ? ' --open' : ' ');
  }
  var outRef = exec(cmd, { maxBuffer: 500 * 1024 },
    function (err, stdout, stderr) {
      if (err) {
        return console.log(err);
      }
    });
  outRef.stdout.pipe(process.stdout);
  outRef.stderr.pipe(process.stderr);
}
