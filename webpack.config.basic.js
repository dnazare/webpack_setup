'use strict';

var webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var ENV = process.env.npm_lifecycle_event;
var isTest = ENV === 'test' || ENV === 'test-watch';
var isProd = ENV === 'build';

console.log(ENV);

module.exports = {
  entry: isTest ? void 0 : { app: './src/app/index.js' },

  output: isTest ? {} : {
    publicPath: isProd ? '' : 'http://localhost:8081/',
    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader, 
          "css-loader",
          "less-loader"
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader',
        options: {
          url: false
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/app/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ],
  devServer: {
    contentBase: './src/public',
    stats: 'minimal',
    proxy: {
      "/StarsOne/*": {
        "context": "/enterprise/",
        "host": "localhost",
        "target": "https://trunk.csstars.com",
        "port": 80,
        "secure": false,
        "changeOrigin": true,
        "pathRewrite": {
          "^/StarsOne": "/enterprise"
        },
        "headers": {
          "Stars-Authentication": "E321TEST_TRUNK:StarsAdmin:Stars@5me:MCT"
        }
      },
      "/nodejs/*": {
        "host": "localhost",
        "context": "/enterprise/nodejs/",
        "target": "https://trunk.csstars.com",
        "port": 80,
        "secure": false,
        "changeOrigin": true,
        "xforward": false
      }
    }
  }
};